
def load_log_file(file_path):
    with open(file_path, 'r', encoding='u8') as f:
        return f.read()
